<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    //CeedboxProjectHealthCheck config
    //This acts as a key to allow the the /health endpoints to work
    'ceedbox_health_id' => env('CEEDBOX_HEALTH_ID', null),
    'ceedbox_health_tables_json' => env('CEEDBOX_HEALTH_TABLES_JSON', null),

    'connection' => env('HEALTH_CHECK_TABLE_CONTENT_CONNECTION_DRIVER', 'mysql'),
    'period_hours' => env('HEALTH_CHECK_TABLE_CONTENT_PERIOD_HOURS', 24),
    'period_column' => env('HEALTH_CHECK_TABLE_CONTENT_PERIOD_COLUMN', 'created_at'),

    'error_when_no_records' => env('HEALTH_CHECK_ERROR_WHEN_NO_RECORDS', true),
    'notify' => env('HEALTH_CHECK_NOTIFY', false),
];
