<?php

namespace Ceedbox\CeedboxProjectHealthCheck;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DatabaseHealthCheck
{
    public function testConnections(): array
    {
        $response = [];
        foreach (config('database.connections') as $dbConnectionName => $databaseCredentials) {
            if (
                isset($databaseCredentials['host']) == false || $databaseCredentials['host'] == '' ||
                isset($databaseCredentials['port']) == false || $databaseCredentials['port'] == '' ||
                isset($databaseCredentials['username']) == false || $databaseCredentials['username'] == '' ||
                isset($databaseCredentials['password']) == false || $databaseCredentials['password'] == ''
            ) {
                continue;
            }
            $response[] = $this->checkDefaultConnection($dbConnectionName);
        }

        return $response;
    }

    /**
     * checkDefaultConnection
     *
     * @param  string $connectionName
     * @return array
     */
    public function checkDefaultConnection($connectionName = 'mysql'): array
    {
        try {
            $dbconnect = DB::connection($connectionName)->getPDO();
            $response[$connectionName] =  true;
        } catch (Exception $e) {
            Log::error($e);
            $response[$connectionName] = false;
        }
        return $response;
    }
}

