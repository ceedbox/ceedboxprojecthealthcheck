<?php

namespace Ceedbox\CeedboxProjectHealthCheck\Http\Controllers;

use Ceedbox\CeedboxProjectHealthCheck\CeedboxProjectHealthCheck;
use Ceedbox\CeedboxProjectHealthCheck\DatabaseContentCheck;
use Ceedbox\CeedboxProjectHealthCheck\QueueHealthCheck;
use Ceedbox\CeedboxProjectHealthCheck\DatabaseHealthCheck;
use Illuminate\Http\Request;
use PSpell\Config;

class HealthController extends Controller
{
    /**
     * Checks the health of all sections
     */
    public function health(Request $request, $id, CeedboxProjectHealthCheck $CeedboxProjectHealthCheck)
    {
        $CeedboxProjectHealthCheck->idCheck($id);

        $DBHealthCheck = $this->CheckDatabaseConnections();

        return response()->json([
            'DBCheck' => $DBHealthCheck,
            'success' => true,
            'message' => 'ok'
        ]);
    }

    public function checkDatabaseConnections()
    {
        $DBHealthCheck = new DatabaseHealthCheck();
        return $DBHealthCheck->testConnections();
    }

    public function databaseContentCheck(Request $request, $id, $table, CeedboxProjectHealthCheck $CeedboxProjectHealthCheck)
    {
        $CeedboxProjectHealthCheck->idCheck($id);

        $parameters = $request->all();

        $request = new Request();
        $request->setMethod('POST');
        $request->request->add(['table' => $table]);
        foreach ($parameters as $key => $parameter) {
            $request->request->add([$key => $parameter]);
        }

        $response = (new DatabaseContentCheck)->databaseContentCheck($request);

        return response()->json([
            'data' => $response,
            'success' => true,
            'message' => 'ok'
        ]);
    }

    public function databaseContentMonitoring($id, CeedboxProjectHealthCheck $CeedboxProjectHealthCheck)
    {
        $CeedboxProjectHealthCheck->idCheck($id);

        $response = (new DatabaseContentCheck)->databaseContentMonitoring(false);

        $tablesFailed = array_filter($response, function($table) {
            return $table['success'] == false;
        });

        return response()->json([
            'data' => $response,
            'success' => (empty($tablesFailed) ? true : false),
            'message' => (empty($tablesFailed) ? 'ok' : 'Errors found')
        ]);
    }

    public function databaseHealth($id, CeedboxProjectHealthCheck $CeedboxProjectHealthCheck)
    {

        $CeedboxProjectHealthCheck->idCheck($id);

        $DBHealthCheck = $this->checkDatabaseConnections();

        $connectionsFailed = array_filter($DBHealthCheck, function($connection) {
            return array_values($connection)[0] == false;
        });

        return response()->json([
            'data' => $DBHealthCheck,
            'success' => (empty($connectionsFailed) ? true : false),
            'message' => (empty($connectionsFailed) ? 'ok' : 'Some connections failed')
        ]);
    }

    public function checkQueueHealth(QueueHealthCheck $queueHealth)
    {
        return $queueHealth->testQueues();
    }

    public function queueHealth($id, QueueHealthCheck $queueHealth, CeedboxProjectHealthCheck $CeedboxProjectHealthCheck)
    {
        $CeedboxProjectHealthCheck->idCheck($id);

        $queueHealth = $this->checkQueueHealth($queueHealth);

        return response()->json([
            'data' => $queueHealth,
            'success' => (isset($queueHealth['dispatched_job']) && $queueHealth['dispatched_job']['successful'] == true ? true : false),
            'message' => (isset($queueHealth['dispatched_job']) && $queueHealth['dispatched_job']['successful'] == true ? 'ok' : 'Job failed')
        ]);
    }
}
