<?php

namespace Ceedbox\CeedboxProjectHealthCheck;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CeedboxProjectHealthCheck
{
    public function idCheck($id)
    {
        if ($id == null) {
            abort(404);
        }

        if (config('landlord.estate_agent_id') != null && $id == config('landlord.estate_agent_id')) {
            return true;
        }

        if (config('ceedbox project health check.ceedbox_health_id') != null && $id == config('ceedbox project health check.ceedbox_health_id')) {
            return true;
        }
        abort(404);
    }
}


