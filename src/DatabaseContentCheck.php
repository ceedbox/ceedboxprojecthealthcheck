<?php

namespace Ceedbox\CeedboxProjectHealthCheck;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class DatabaseContentCheck
{
    // this uses json env
    public function databaseContentMonitoring()
    {
        $tablesJsonObject = config('ceedbox project health check.ceedbox_health_tables_json');

        $response = [];
        if (! empty($tablesJsonObject)) {
            $tablesArray = json_decode($tablesJsonObject);

            // get into the correct nested object
            foreach ($tablesArray as $tables) {
                foreach ($tables as $table) {
                    // build up request
                    $request = new Request();
                    $request->setMethod('POST');
                    foreach ($table as $key => $parameter) {
                        $request->request->add([$key => $parameter]);
                    }

                    $response[] = (new DatabaseContentCheck)->databaseContentCheck($request, true);
                }
            }
        }

        return $response;
    }

    /**
     * Generic database content check function.
     * can either pass up in a request or hit the function directly
     *
     * @param Request $request
     * @return void
     */
    public function databaseContentCheck(Request $request, $monitoring = false)
    {
        // validate the request and build up the query params to pass to tableHasRecords
        $values = $this->databaseContentValidateAndBuild($request);

        $table = $values['table'];
        $connection = $values['connection'];
        $periodHours = $values['periodHours'];
        $errorWhenNoRecords = $values['errorWhenNoRecords'];
        $params = $values['params'];

        // connection defaults to mysql otherwise variable
        $checkResult = $this->tableHasRecords($table, $params, $connection);

        // check if errors and if so then return errors found and sets records as records if any
        $errors = $checkResult['errors'] ?? false;
        $records = $checkResult['records'];
        if ($errors) {
            Log::error('Health check error: ' . ($checkResult['message'] ?? ''));

            return response()->json([
                'success' => false,
                'message' => 'Errors found'
            ], 500);
        }

        // return the info
        $success = true;

        if ($errorWhenNoRecords) {
            if ($records == 0) {
                $success = false;
            }
        } else {
            if ($records > 0) {
                $success = false;
            }
        }

        if ($monitoring) {
            // if using monitoring route this will be true and return
            return [
                'table' => $table,
                'data' => $values,
                'records' => $records,
                'success' => $success
            ];
        } else {
            if ($success) {
                return response()->json([
                    'success' => true,
                    'message' => "Records found in the last $periodHours hours"
                ], 200);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => "Records found in the last $periodHours hours"
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Conditions not met to return anything and or nothing to return'
        ], 200);
    }

    // per table build up either the default or custom query for monitoring on that table
    public function databaseContentValidateAndBuild(Request $request)
    {
        $request->validate([
            'table' => 'required|string',
            'conditions' => 'nullable|array',
            'connection' => 'nullable|string',
            'period_hours' => 'nullable|integer',
            'period_column' => 'nullable|string',
            'error_when_no_records' => 'nullable|boolean',
            'notify' => 'nullable|boolean'
        ]);

        // table alone results in default 24 hour created_at query parameters
        $table = trim($request->input('table'));

        // custom query parameters
        $conditions = $request->input('conditions', []);
        $connection = $request->input('connection', config('ceedbox project health check.connection'));
        $periodHours = intval($request->input('period_hours', config('ceedbox project health check.period_hours')));
        $periodColumn = $request->input('period_column', config('ceedbox project health check.period_column'));

        // response parameters
        $errorWhenNoRecords = $request->input('error_when_no_records', config('ceedbox project health check.error_when_no_records'));
        $notify = $request->input('notify', config('ceedbox project health check.notify'));

        // build queries
        $params = [];
        if (count($conditions) > 0) {
            // custom query
            foreach ($conditions as $key => $condition) {
                $params = array_merge($params, [$condition]);
            }
        } else {
            // default
            $params[] = ['raw', "$periodColumn > DATE_SUB(NOW(), INTERVAL $periodHours HOUR)"];
        }

        $data = [
            'params' => $params,
            'table' => $table,
            'conditions' => $conditions,
            'connection' => $connection,
            'periodHours' => $periodHours,
            'periodColumn' => $periodColumn,
            'errorWhenNoRecords' => $errorWhenNoRecords,
            'notify' => $notify
        ];

        return $data;
    }

    /**
     * Checks if a table has records based on the $conditions array:
     * [type, column, operator, value]
     *
     * type: where, or, and, null, notnull, in, notin, raw
     * column: the database table field name (not required when type = 'raw')
     * operator: '=','>','<','>=','<=','<>'
     * value: the database table field value
     *
     * example with multiple conditions:
     *
     * ['where', 'status', 'error'],
     * ['and', 'created_at', '>' '2023-05-23'],
     * ['or', 'enabled', '=' '1']
     *
     * example with raw SQL condition:
     *
     * ['raw', 'created_at > DATE_SUB(NOW(), INTERVAL 24 HOUR)'],
     * ['and', 'enabled', '1']
     *
     * On error, returns an array with the error message:
     *
     * ['errors' => true, 'message' => 'Value must be a string']
     *
     * On success, returns an array with the number of record found in the table:
     *
     * ['errors' => false, 'records' => 5]
     *
     * @param string $tableName
     * @param array $conditions
     * @param string $connectionName
     * @return array
     */
    public function tableHasRecords($tableName, $conditions, $connectionName): array
    {
        $query = DB::connection($connectionName)->table($tableName);

        foreach ($conditions as $condition) {
            $type = $column = $operator = $value = null;

            $type = strtolower($condition[0]) ?? null;
            if (empty($type)) {
                return ['errors' => true, 'message' => 'Type cannot be empty'];
            }

            if ($type == 'raw') {
                $value = $condition[1] ?? null;
                if (empty($value)) {
                    return ['errors' => true, 'message' => 'Value is empty and must be a string'];
                };
            } else {
                $column = $condition[1] ?? null;

                if (isset($condition[3])) {
                    $operator = $condition[2];
                    $value = $condition[3];
                } elseif (isset($condition[2])) {
                    $operator = '=';
                    $value = $condition[2];
                }
            }

            switch (strtolower($type)) {
                case 'or':
                case 'orwhere':
                    $type = 'orWhere';
                    break;
                case 'whereraw':
                case 'raw':
                    $type = 'whereRaw';
                    break;
                case 'wherenull':
                case 'null':
                    $type = 'whereNull';
                    break;
                case 'wherenotnull':
                case 'notnull':
                    $type = 'whereNotNull';
                    break;
                case 'wherein':
                case 'in':
                    $type = 'whereIn';
                    break;
                case 'notin':
                case 'in':
                    $type = 'whereNotIn';
                    break;
                case 'where':
                case 'and':
                    $type = 'where';
                    break;
                default:
                    $type = 'where';
                    break;
            }

            // validations
            if (empty($column) && $type != 'whereRaw') {
                return ['errors' => true, 'message' => "Column is empty for type $type"];
            };

            if (!in_array($operator, ['=', '>', '<', '>=', '<=', '<>']) && $type != 'whereRaw') {
                return ['errors' => true, 'message' => "Invalid operator ($operator) for type $type"];
            }

            if (($type == 'whereIn' || $type == 'whereNotIn') && !is_array($value)) {
                return ['errors' => true, 'message' => "Value must be an array for type $type"];
            }

            if ($type == 'whereRaw') {
                $query->{$type}($value);
                continue;
            }

            if ($type == 'whereNull' || $type == 'whereNotNull') {
                $query->{$type}($column);
                continue;
            }

            if ($type == 'whereIn' || $type == 'whereNotIn') {
                $query->{$type}($column, $value);
                continue;
            }

            if (empty($operator)) {
                $query->{$type}($column, $value);
            } else {
                $query->{$type}($column, $operator, $value);
            }
        }

        // either get for database content monitoring and check or delete for database content delete
        try {
            $result = $query->get();
        } catch (Exception $e) {
            Log::error($e);
            return ['errors' => true];
        }

        return ['errors' => false, 'records' => $result->count()];
    }
}
