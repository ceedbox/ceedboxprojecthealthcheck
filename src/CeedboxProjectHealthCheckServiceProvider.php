<?php

namespace Ceedbox\CeedboxProjectHealthCheck;

use Illuminate\Support\ServiceProvider;
use Ceedbox\CeedboxProjectHealthCheck\Console\Commands\DeleteOldRecords;

class CeedboxProjectHealthCheckServiceProvider extends ServiceProvider
{
    protected $commands = [
        DeleteOldRecords::class,
    ];
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ceedbox project health check');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'ceedbox project health check');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('ceedbox project health check.php'),
            ], 'config');


            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/ceedbox project health check'),
            ], 'views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/ceedbox project health check'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/ceedbox project health check'),
            ], 'lang');*/

            // Registering package commands.
            $this->commands($this->commands);

        }
        $this->loadRoutesFrom(__DIR__ . '../../routes/web.php');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'ceedbox project health check');

        // Register the main class to use with the facade
        $this->app->singleton('ceedbox project health check', function () {
            return new CeedboxProjectHealthCheck;
        });
    }
}
