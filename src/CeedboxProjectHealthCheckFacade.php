<?php

namespace Ceedbox\CeedboxProjectHealthCheck;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Ceedbox\CeedboxProjectHealthCheck\Skeleton\SkeletonClass
 */
class CeedboxProjectHealthCheckFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ceedbox project health check';
    }
}

