<?php

namespace Ceedbox\CeedboxProjectHealthCheck\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DeleteOldRecords extends Command
{
  protected $signature = 'records:delete-old';
  protected $description = 'Delete old records from specified tables based on configuration';

  public function handle()
  {
    $config = config('healthCheck.delete_old_records');

    foreach ($config as $table => $months) {
      $this->info("Deleting records from {$table} older than {$months} months.");
      DB::table($table)->where('created_at', '<', now()->subMonths($months))->delete();
    }

    $this->info('Old records deletion completed.');
  }
}