<?php

namespace Ceedbox\CeedboxProjectHealthCheck;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class QueueHealthCheck
{
    /**
     * Checks the queue length and attempts to dispatch a job
     * waits 30 seconds and attempts to find the
     * response in the laravel logs
     *
     */
    public function testQueues(): array
    {

        $carbon1 = Carbon::now()->format('d/m/Y H:i:s');
        $Qcount1 = $this->getQueueLength();

        $stringToFind = Str::random(15);

        $latestJobCreated = $this->getLatestJobsCreatedAtDate();

        $dispatched = $this->dispatchQueueHealthJob($stringToFind);

        sleep(30);

        $jobStringFound = $this->getLaravelLogsAndCheckForString($stringToFind);

        $carbon2 = Carbon::now()->format('d/m/Y H:i:s');
        $Qcount2 = $this->getQueueLength();

        // if the dispatched_job['successful'] is false it could be
        // due to the job not being finished so we return the
        // string original so we return the string original
        // string so the user can go direct into the logs
        // and find it afterwards.
        return [
            'jobs_queue_length_earlier' => [
                'count' => $Qcount1,
                'ran_at' => $carbon1
            ],
            'jobs_queue_length_now' => [
                'count' => $Qcount2,
                'ran_at' => $carbon2
            ],
            'dispatched_job' => [
                'string' => $stringToFind,
                'created' => $dispatched != false ? true : false,
                'successful' => $jobStringFound,
            ],
            'most_recently_created_job' => $latestJobCreated,
        ];
    }

    /**
     * Gets the length of the queues
     */
    public function getQueueLength()
    {
        return Queue::size();
    }

    /**
     * Gets the length of the queues
     */
    public function getLatestJobsCreatedAtDate()
    {
        $job = DB::table('jobs')->select(['created_at'])->orderByDesc('created_at')->first();
        if ($job != null && $job != false) {
            return Carbon::createFromTimestamp($job->created_at)->format('d/m/Y H:i:s');
        } else {
            return null;
        }
    }

    /**
     * Dispatches a job to the queue table containing a string to log in the logs file.
     */
    public function dispatchQueueHealthJob($stringToFind)
    {
        return dispatch(new TestJob('CEEDBOX-HEALTH-QUEUE-STRING-TO-CHECK' . $stringToFind))->onQueue('default') ?? false;
    }

    /**
     * Finds and reads the logs file and searches for a specified string
     */
    public function getLaravelLogsAndCheckForString($stringToFind)
    {
        $date = Carbon::now();
        $currentDate = $date->format('Y-m-d');
        $storage = storage_path('logs');
        $filePathDefault = $storage . "/laravel.log";
        $filePathToday = $storage . "/laravel-{$currentDate}.log";

        $defaultLog = File::exists($filePathDefault);
        $todaysLog = File::exists($filePathToday);

        //no log file for today and no default log file
        if ($defaultLog == false && $todaysLog == false) {
            Log::error([__METHOD__, 'no log file found']);
            return false;
        }

        //check todays log as that should really exist.
        //if no logs for today check the default log
        if ($todaysLog == false && $defaultLog == true) {
            $filePathToday = $filePathDefault;
        }

        //get the file
        $file = File::get($filePathToday);

        //look for the string we passed in
        return Str::contains($file, $stringToFind);
    }
}
