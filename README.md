# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/ceedbox/ceedbox project health check.svg?style=flat-square)](https://packagist.org/packages/ceedbox/ceedbox project health check)
[![Total Downloads](https://img.shields.io/packagist/dt/ceedbox/ceedbox project health check.svg?style=flat-square)](https://packagist.org/packages/ceedbox/ceedbox project health check)
![GitHub Actions](https://github.com/ceedbox/ceedbox project health check/actions/workflows/main.yml/badge.svg)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require ceedbox/ceedboxprojecthealthcheck
```

## Usage

```php
// access health via web routes or alternatively 
//  $cphCheck = new CeedboxProjectHealthCheck();
//  return $cphCheck>testConnections()
```

### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email tom.fisher@ceedbox.com instead of using the issue tracker.

## Credits

-   [Tom Fisher](https://github.com/ceedbox)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
