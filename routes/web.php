<?php

use Illuminate\Support\Facades\Route;
use Ceedbox\CeedboxProjectHealthCheck\Http\Controllers\HealthController;

Route::get('/health/{reference}', [HealthController::class, 'health']);
Route::get('/database-health/{reference}', [HealthController::class, 'databaseHealth']);
Route::get('/queue-health/{reference}', [HealthController::class, 'queueHealth']);

Route::get('/database-content-check/{reference}/{table}', [HealthController::class, 'databaseContentCheck']);
Route::get('/database-content-monitor/{reference}', [HealthController::class, 'databaseContentMonitoring']);
